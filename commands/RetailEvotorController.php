<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\helper\EvotorRetailSyncer;
use RetailCrm\ApiClient;
use RetailCrm\Exception\CurlException;
use SebastianBergmann\CodeCoverage\Report\PHP;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class RetailEvotorController extends Controller
{
    /**
     * @return string
     */
    public function actionRaikin()
    {
        $client = new ApiClient(
            'https://letique.retailcrm.ru',
            'ключ',
            ApiClient::V5
        );

        $syncer = new EvotorRetailSyncer('ключ', 'ключ', $client->request, 'sklad-raikin');
    }

    public function actionHimki()
    {
        $ch = curl_init('https://api.evotor.ru/stores/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: ключ',
        ));
        $all = 0;
        $result = curl_exec($ch);
        $shopName = json_decode($result)->items[0]->name;
        $shopAddress = json_decode($result)->items[0]->address;

        $ch = curl_init('https://api.evotor.ru/stores/ключ/documents?type=SELL&since=' . (strtotime(gmdate('Y-m-d', strtotime('-1 days'))) * 1000) . '&until=' . (strtotime(gmdate('Y-m-d'))) * 1000);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: ключ',
        ));

        $result = curl_exec($ch);
        $models = [];
        $count = 0;
        $dateAll = null;
        $sklad = [];
        foreach (json_decode($result)->items as $id => $item) {
            $models[$item->id]['price'] = 0;
            $user_id = $item->user_id;
            $date = strtotime($item->close_date) + $item->time_zone_offset / 1000;
            $date = gmdate('Y-m-d H:i:s', $date);
            $dateCheck = gmdate('Y-m-d ', strtotime($item->close_date));
            if ($dateCheck != $dateAll) {
                $dateAll = $dateCheck;
                $count = 0;
            } else {
                $count++;
            }
            foreach ($item as $w => $body) {
                if (is_object($body)) {
                    if (isset($body->positions)) {
                        foreach ($body->positions as $key => $model) {
                            if ($model->product_id) {
                                $models[$item->id]['items'][$key]['quantity'] = $model->quantity;
                                if (isset($model->bar_code)) {
                                    $sklad[$model->bar_code]['externalId'] = $model->bar_code;
                                    $sklad[$model->bar_code]['stores'] = [
                                        'sklad-mega-himki' => [
                                            'code' => 'sklad-mega-himki',
                                            'available' => $model->initial_quantity
                                        ]
                                    ];
                                    $models[$item->id]['items'][$key]['offer'] = ['externalId' => $model->bar_code];
                                }
                                $models[$item->id]['items'][$key]['productName'] = $model->product_name;
                                $models[$item->id]['items'][$key]['measure_name'] = $model->measure_name;
                                $models[$item->id]['items'][$key]['initialPrice'] = $model->result_price;
                                $models[$item->id]['items'][$key]['status'] = 'saled';
                                $models[$item->id]['price'] = $models[$item->id]['price'] + $model->result_sum;
                                $models[$item->id]['date'] = $date;
                                $models[$item->id]['id'] = $count;
                                $models[$item->id]['items'][$key]['properties'] =
                                    [
                                        [
                                            'name' => 'Штрих-код',
                                            'value' => isset($model->bar_code) ? $model->bar_code : 'Нету'
                                        ],
                                        [
                                            'name' => 'user_id_evotor',
                                            'value' => $user_id
                                        ],
                                        [
                                            'name' => 'Название магазина',
                                            'value' => $shopName
                                        ],
                                        [
                                            'name' => 'Адрес магазина',
                                            'value' => $shopAddress
                                        ],

                                    ];
                            }
                        }
                    }
                }
            }

        }
        $client = new ApiClient(
            'https://letique.retailcrm.ru',
            'ключ',
            ApiClient::V5
        );
        foreach ($models as $key => $model) {
            try {
                $response = $client->request->ordersCreate(array(
                    'firstName' => $shopName,
                    'orderMethod' => 'offline',
                    'createdAt' => $model['date'],
                    'number' => date('dmy', strtotime($model['date'])) . '/' . ((int)$model['id'] + 1) . 'МЕГАХимки',
                    'status' => 'complete',
                    'items' => $model['items'],
                ), 'megahimki');

                foreach ($sklad as $bar_code => $value) {
                    $client->request->storeInventoriesUpload([$value], 'letique-ru');
                }
                $client->request->ordersPaymentCreate(
                    [
                        'order' => [
                            'id' => $response->id
                        ],
                        'amount' => $model['price'],
                        'paidAt' => $model['date'],
                        'status' => 'paid',
                        'type' => 'bank-card',
                    ], 'megahimki'
                );
            } catch (CurlException $e) {
                echo "Connection error: " . $e->getMessage();
            }
            VarDumper::dump($model['id']);
        }
        return '200';
    }
}
