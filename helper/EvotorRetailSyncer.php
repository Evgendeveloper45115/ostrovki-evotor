<?php

namespace app\helper;

use yii\helpers\VarDumper;

class EvotorRetailSyncer
{

    protected $storeKey;
    protected $authKey;
    protected $retailShopId;
    protected $retailClient;


    public function __construct($storeKey, $authKey, $retailClient, $retailShopId)
    {
        $this->storeKey = $storeKey;
        $this->authKey = $authKey;
        $this->retailClient = $retailClient;
        $this->retailShopId = $retailShopId;
    }

    public function syncInitialStock()
    {
        $products = $this->getInitialStock();

        return $this->syncRetailStock($products);

    }

    public function syncYesterdayStock()
    {
        $products = $this->getYesterdayStock();
        return $this->syncRetailStock($products);
    }

    protected function syncRetailStock($products)
    {
        $retailProducts = $this->convertToRetailProducts($products);
        return $this->retailClient->storeInventoriesUpload($retailProducts, 'letique-ru');
    }


    protected function getYesterdayStock()
    {

        $yesterday = date("Y-m-d", strtotime("-1 days"));
        $docs = $this->getEvotorDocsForPeriod($yesterday, $yesterday);
        $products = $this->getEvotorProducts();
        $products = $this->setLeftProducts($docs, $products, false);

        return $products;
    }

    protected function getInitialStock()
    {

        $products = $this->getEvotorProducts();
        $storeOpenDate = strtotime($this->getStoreOpenDate());

        $index = 0;

        while (true) {

            $fromDay = 30 * $index + 30;
            $untilDay = 30 * $index + 1;

            $from = date("Y-m-d", strtotime("-{$fromDay} days"));
            $until = date("Y-m-d", strtotime("-{$untilDay} day"));
            $index++;

            if (strtotime($until) < $storeOpenDate) {
                break;
            }

            $docs = $this->getEvotorDocsForPeriod($from, $until);

            if (!isset($docs->items)) {
                continue;
            }

            $products = $this->setLeftProducts($docs, $products);

        }

        return $products;
    }

    protected function convertToRetailProducts(array $stock)
    {
        foreach ($stock as $st) {
            if (!isset($st['barcode'])) continue;

            $storeElement = [
                'externalId' => $st['barcode'],
                'stores' => []
            ];
            $storeElement['stores'][$this->retailShopId] = [
                'code' => $this->retailShopId,
                'available' => $st['left'],
                'name' => $st['name']
            ];

            $storeStock[] = $storeElement;
        }

        return $storeStock;
    }

    protected function setLeftProducts($docs, $products, $wholeStock = true)
    {
        $stock = [];
        $items = $docs->items;
        for ($i = count($items) - 1; $i >= 0; $i--) {
            $item = $items[$i];
            $type = $item->type;

            if (!isset($item->body->positions)) continue;
            foreach ($item->body->positions as $key => $position) {

                if (!isset($position->product_id) || isset($stock[$position->product_id])) continue;
                if (!isset($position->settlement_method->type) || !in_array($position->settlement_method->type, ['CHECKOUT_FULL', 'CHECKOUT_PARTIAL', 'CREDIT_PASS', 'CREDIT_CHECKOUT'])) continue;

                if (isset($products[$position->product_id])) {
                    $stock[$position->product_id] = $products[$position->product_id];
                } else {
                    $stock[$position->product_id] = [];
                }
                $stock[$position->product_id]['left'] = $position->initial_quantity;
                if ($position->initial_quantity < 0 || $position->initial_quantity > 99999) {
                    VarDumper::dump($position);
                    VarDumper::dump($position->initial_quantity . PHP_EOL);
                }

                if ($type === 'SELL') {
                    $stock[$position->product_id]['left'] -= $position->quantity;
                } elseif ($type === 'ACCEPT' || $type === 'PAYBACK') {
                    $stock[$position->product_id]['left'] += $position->quantity;
                }


                if ($products && isset($products[$position->product_id])) {

                    $products[$position->product_id]['left'] = $stock[$position->product_id]['left'];
                }

            }
        }

        return $wholeStock ? $products : $stock;
    }

    protected function getEvotorDocsForPeriod($from, $to)
    {

        $from = strtotime($from) * 1000;
        $until = strtotime($to . ' 23:59:59') * 1000;
        $docUrl = "https://api.evotor.ru/stores/{$this->storeKey}/documents?since={$from}&until={$until}";

        return $this->request($docUrl);
    }

    protected function getEvotorProducts()
    {
        $productUrl = "https://api.evotor.ru/stores/{$this->storeKey}/products";

        $items = $this->request($productUrl)->items;
        $products = [];
        foreach ($items as $item) {
            if (isset($item->barcodes[0])) {
                $products[$item->id] = [
                    'barcode' => $item->barcodes[0],
                    'name' => $item->name,
                    'left' => 0
                ];
            } else {
                $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'no_bar_code';
                if (!is_file($file)) {
                    fopen($file, "w", true);
                }
                file_put_contents($file, json_encode($item), FILE_APPEND);
            }
        }

        return $products;
    }

    protected function getStoreOpenDate()
    {
        $store = $this->request('https://api.evotor.ru/stores/');
        return $store->items[0]->created_at;
    }

    protected function request($url)
    {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: ' . $this->authKey,
        ));

        $response = curl_exec($ch);
        return json_decode($response);
    }
}

