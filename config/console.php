<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'aliases' => [
        '@webroot' => dirname(dirname(__FILE__)) . '',
    ],
    'components' => [
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'app\models\User',// class that implements IdentityInterface
            //'enableAutoLogin' => true,
        ],
        'session' => [ // for use session in console application
            'class' => 'yii\web\Session'
        ],
        'imageResizer' => [
            'class'                => 'app\components\MyResizer',
            // directory with images
            'dir'                  => '/uploads',
            // image sizes. If 'suffix' not set, than width and height be used for suffix name.
            'sizes'                => [
                // in this case height will be calculated automatically
                ['width' => 600, 'height' => null, 'suffix' => ''],

            ],
            // handle directory recursively
            'recursively'          => true,
            // enable rewrite thumbs if its already exists
            'enableRewrite'        => true,
            // array|string the driver to use. This can be either a single driver name or an array of driver names.
            // If the latter, the first available driver will be used.
            'driver'               => ['gmagick', 'imagick', 'gd2'],
            // image creation mode.
            'mode'                 => 'inset',
            // enable to delete all images, which sizes not in $this->sizes array
            'deleteNonActualSizes' => false,
            // background transparency to use when creating thumbnails in `ImageInterface::THUMBNAIL_INSET`.
            // If "true", background will be transparent, if "false", will be white color.
            // Note, than jpeg images not support transparent background.
            'bgTransparent' => false,
            // want you to get thumbs of a fixed size or not. Has no effect, if $mode set "outbound".
            // If "true" then thumbs will be the exact same size as in the $sizes array.
            // The background will be filled with white color.
            // Background transparency is controlled by the parameter $bgTransparent.
            // If "false", then thumbs will have a proportional size.
            // If the size of the thumbs larger than the original image,
            // the thumbs will be the size of the original image.
            'fixedSize' => false,
        ],

        'mailer' => [
            'class' => 'zyx\phpmailer\Mailer',
            'viewPath' => '@app/mail',
            'htmlView' => '@app/mail',
            'useFileTransport' => false,
            'messageConfig' => [
                'from' => 'admin@lerchek.ru'
            ],
            'config' => [
                'mailer' => 'smtp',
                'host' => 'smtp.yandex.ru',
                'port' => '465',
                'smtpsecure' => 'ssl',
                'smtpauth' => true,
                'username' => 'admin@lerchek.ru',
                'password' => 'gfdsgre23G',
            ],
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];
